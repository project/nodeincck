A CCK field to insert another node into the node edit form and display. This
is mainly to provide a way to insert some lenthy formatted text such as legalese.

Theming:

The node.tpl.php template can be overriden with:

node-nodeincck.tpl.php
node-nodeincck-<FIELD_NAME>.tpl.php
node-nodeincck-<NODE_TYPE>.tpl.php
node-nodeincck-<FIELD_NAME>-<NODE_TYPE>.tpl.php

Warning:

Be carefule not to create circular reference with this or the nodereference module
that directly or indirectly refer back to the same node. This will cause an
endless loop and PHP stack overflow. You will just get a white screen.